import HtmlWebpackPlugin from 'html-webpack-plugin';
import BookService from './services/bookService'
const BookService = new BookService();

import { format } from 'timeago.js';
class UI {


    async renderBooks() {
       const books = await bookService.getBooks();
       const booksCardContainer = document.getElementsById('books-cards');
       booksCardContainer.innerHTML = '';
       books.forEach(book => {
          const div = document.createElement('div');
          div.className ='';
          div.innerHTML = '
              <div class="card m-2>
                   <div class="row">
                    </div class="col-md-4">
                           <Image src="http://localhost:3000${book.imagePath}"alt="" class= "img-fluid"/>
                    </div>
                <div class="col-md-8">
                     <div class="card-block px-2">
                        <h4 class="card-title">${book.title}</h4>
                        <p class="card-text">${book.author}</p>
                         <a href="#" class="btn btn-danger delete"> _id="${book._id}"x</a>
                     </div>
                   <div class="card-footer">
                         ${format(book.created_at)}
               <div>
            </div>                  
          ;' 
          booksCardContainer.appendChild(div); 
        })

   async addANewBook() {
     await BookService.postBook(book);
     this.clearBookForm();
     this.renderBooks();
    }
        

    clearBookform() {
        document.getElementsById('book-form').requestFullscreem();
    }

    renderMessage() {}

    DeleteBook() {}

}

export default UI